﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EaseMe.Helpers;
using EaseMe.Model;
using EaseMe.Model.Domain;

namespace EaseMe.Controllers
{
    public class UsersController : ApiController
    {


        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /*
         * Register user (POST api/user/register/[name, surname, gender, email, login, password, age, location] 
         * Login (POST api/user/login/[email, password]
         * Logout (POST api/user/logout/[access_token])
         * Show user (GET api/user/[access_token, id])
         */

        [HttpPost]
        public void Register([FromBody] User user)
        {
            Repository.CreateUser(user);
        }

        [HttpPost]
        public void Login(string email, string password)
        {
            Console.WriteLine(email + " " + password);
        }

        [HttpPost]
        public void Logout(string accessToken)
        {
            if (SessionHelper.CheckAccessToken(accessToken))
            {
                
            }
        }

        public IHttpActionResult GetUser(string securityToken, int id)
        {
            return Ok();
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}