﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EaseMe.Helpers;
using EaseMe.Model;
using EaseMe.Model.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EaseMe.Controllers
{
    public class TicketsController : ApiController
    {
        public const string K_TICKETS_ARRAY_TAG = "tickets";
        /*
         * Purchase tickets (POST api/tickets/purchase/[access_token, tickets, payment info])
         * Show purchased tickets (GET api/tickets/[access_token])
         */

        public class TicketStructure
        {
            public int TicketSourceId { get; set; }
            public int RouteId { get; set; }
        }

        public class PurchaseInfoRequest
        {
            public List<TicketStructure> Tickets { get; set; }
        }


        [HttpPost]
        public IHttpActionResult Purchase(string accessToken, [FromBody]dynamic paymentInfoJson)
        {
            JToken token = paymentInfoJson as JToken;
            if (token == null || accessToken == null)
            {
                return NotFound();
            }

            String json = (paymentInfoJson as JToken).ToString();
            PurchaseInfoRequest routeData = JsonConvert.DeserializeObject<PurchaseInfoRequest>(json);

            if (PurchaseHelper.PurchaseTickets(routeData))
            {
                Console.WriteLine("Tickets purchased");
                return Ok();
            }

            return NotFound();
        }

        // receive usertickets (GET api/tickets/[accessToken])
        public IEnumerable<Ticket> Get(string accessToken)
        {
            return Repository.GetTickets(accessToken);
        }
        
    }
}