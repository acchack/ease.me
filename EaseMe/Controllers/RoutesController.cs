﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EaseMe.Helpers;
using EaseMe.Model;
using GoogleMapsApi.Entities.Directions.Request;
using GoogleMapsApi.Entities.Directions.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EaseMe.Controllers
{
    public class RoutesController : ApiController
    {
        public class RouteRequestInfo
        {
            public string StartPoint { get; set; }
            public string EndPoint { get; set; }

            public int TransportType { get; set; }
            public DateTime Time { get; set; }
            public int TimeMode { get; set; }

            public List<string> Waypoints;
        }

        /*
         * Calculate route (POST api/routes/[points, types, timestamps, waypoints])
         */

        [HttpPost]
        public IHttpActionResult Post([FromBody]dynamic routeRequest)
        {
           JToken token = routeRequest as JToken;

            if (token == null)
            {
                return NotFound();
            }

           RouteRequestInfo routeData = JsonConvert.DeserializeObject<RouteRequestInfo>(token.ToString());

            DirectionsResponse response = GoogleMapsApiHelpers.GetDirectionResponse(routeData);

            return Ok(response);
            //return null;
        }
    }
}