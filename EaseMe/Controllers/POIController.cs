﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using EaseMe.Helpers;
using EaseMe.Model;
using EaseMe.Model.Domain;
using GoogleMapsApi.Entities.Places.Response;
using Newtonsoft.Json.Linq;

namespace EaseMe.Controllers
{
    public class PoiController : ApiController
    {

        public const string K_POI_ARRAY_NAME_TAG = "poi";
        // GET api/<controller>
        // return POIs

        /*
         * Show layers (GET api/poi/layers/[access_token])
         * Show POI (GET api/poi/[access_token, layers])
         */

        [HttpPost]
        [Route("api/poi")]
        public IHttpActionResult GetPoi(string accessToken, [FromBody]dynamic maplayerJson)
        {

            JObject jObject = JObject.Parse(maplayerJson);
            JToken jToken = jObject.GetValue(K_POI_ARRAY_NAME_TAG);

            var array = JArray.Parse( jToken.Value<string>() ).Select(x => (int)x ).ToArray();

            IList<Maplayer> availableLayers = Repository.GetMaplayersFromDb(accessToken != null);



            PlacesResponse response = new PlacesResponse();

            return NotFound();
        }

        // return Layers
        [HttpGet]
        [Route("api/poi/layers")]
        public IEnumerable<Maplayer> Layers(string accessToken)
        {
            return Repository.GetMaplayersFromDb(SessionHelper.CheckSession(accessToken));
        }
    }
}