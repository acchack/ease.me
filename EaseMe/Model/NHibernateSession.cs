﻿using System.Web;
using NHibernate;
using NHibernate.Cfg;

namespace EaseMe.Model
{
    public class NHibernateSession
    {

        private static NHibernateSession instance = null;
        private static readonly object Padlock = new object();
        public static ISessionFactory SessionFactory;

        NHibernateSession()
        {

        }

        public static NHibernateSession Instance
        {
            get
            {
                lock (Padlock)
                {
                    return instance ?? (instance = new NHibernateSession());
                }
            }
        }

        public static ISession OpenSession()
        {
            var configuration = new Configuration();
            
            //Main Configuration File
            string configurationPath;
            configurationPath = 
                            HttpContext.Current.Server.MapPath(@"~\Model\hibernate.cfg.xml");
                       
            configuration.Configure(configurationPath);
            
            //Entity Configuration Files
            var achievementsConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\achievements.hbm.xml");
            configuration.AddFile(achievementsConfigurationFile);
            var currencyConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\currency.hbm.xml");
            configuration.AddFile(currencyConfigurationFile);
            var interestsConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\interests.hbm.xml");
            configuration.AddFile(interestsConfigurationFile);
            var loginConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\login.hbm.xml");
            configuration.AddFile(loginConfigurationFile);
            var placefeedbackConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\placefeedback.hbm.xml");
            configuration.AddFile(placefeedbackConfigurationFile);
            var placefeedbackmediaConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\placefeedbackmedia.hbm.xml");
            configuration.AddFile(placefeedbackmediaConfigurationFile);
            var sessionConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\session.hbm.xml");
            configuration.AddFile(sessionConfigurationFile);
            var ticketConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\ticket.hbm.xml");
            configuration.AddFile(ticketConfigurationFile);
            var ticketsourceConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\ticketsource.hbm.xml");
            configuration.AddFile(ticketsourceConfigurationFile);
            var ticketstatusConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\ticketstatus.hbm.xml");
            configuration.AddFile(ticketstatusConfigurationFile);
            var transportationmeansConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\transportationmeans.hbm.xml");
            configuration.AddFile(transportationmeansConfigurationFile);
            var userConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\user.hbm.xml");
            configuration.AddFile(userConfigurationFile);
            var userachievementsConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\userachievements.hbm.xml");
            configuration.AddFile(userachievementsConfigurationFile);
            var userpreferencesConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\userpreferences.hbm.xml");
            configuration.AddFile(userpreferencesConfigurationFile);

            var maplayerConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\MapLayer.hbm.xml");
            configuration.AddFile(maplayerConfigurationFile);
            var bikemiConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Model\Mappings\BikeMi.hbm.xml");
            configuration.AddFile(bikemiConfigurationFile);


            SessionFactory = configuration.BuildSessionFactory();
            return SessionFactory.OpenSession();
        }

    }
}