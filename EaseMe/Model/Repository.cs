﻿using System;
using System.Collections.Generic;
using System.Linq;
using EaseMe.Model.Domain;
using NHibernate.Linq;

namespace EaseMe.Model
{
    public class Repository
    {
        public static IList<Maplayer> GetMaplayersFromDb(bool isAuthorized)
        {
            IList<Maplayer> results;
            using (var session = NHibernateSession.OpenSession())
            {
                var queryResults = session.Query<Maplayer>();

                results = isAuthorized ? 
                    queryResults.ToList() 
                    : 
                    queryResults.Where(x => x.isPublic).ToList();
            }
            return results;
        }

        public static IEnumerable<Bikemi> GetBikeMiPoints(bool isAuthorized)
        {
            if (!isAuthorized) return null;
            using (var session = NHibernateSession.OpenSession())
            {
                return session.Query<Bikemi>().ToList();
            }
        }


        public static User GetUserInfoByIdAndSecurityToken(String securityToken,
            String loginName)
        {
            using (var session = NHibernateSession.OpenSession())
            {
                var sessionHandler = session.Query<Session>();
                var sessionFound = sessionHandler.Where(x => x.securitytoken == securityToken)
                    .Any(x => x.user.login.First().loginval == loginName);
                if (!sessionFound) return null;
                var login = session.Query<Login>();
                var loginUser = login.First(x => x.loginval == loginName);
                var user = session.Query<User>().Where(x => x.login.First().id == loginUser.id);
                return user.First();
            }
        }

        public static void CreateUser(User _user)
        {
            using (var session = NHibernateSession.OpenSession())
            {
                var userToSave = new User
                {
                    name = _user.name,
                    surname = _user.surname,
                    gender = _user.gender,
                    email = _user.email,
                    location = _user.location,
                    createdon = DateTime.Now,
                    age = _user.age
                };
                using (var transaction = session.BeginTransaction())
                {
                    session.Save(userToSave);
                    transaction.Commit();
                    var loginToSave = new Login
                    {
                        user = userToSave,
                        createdon = DateTime.Now,
                        loginval = _user.login.First().loginval,
                        password = _user.login.First().password,
                    };
                    session.Save(loginToSave);
                    transaction.Commit();
                }
            }
        }

        public static IEnumerable<Ticket> GetTickets(string accessToken)
        {
            using (var session = NHibernateSession.OpenSession())
            {
                var sessionHandler = session.Query<Session>();
                var sessionFound = sessionHandler.Where(x => x.securitytoken == accessToken);
                var tickets = session.Query<Ticket>()
                    .Where(x => x.user.id == sessionFound.First().user.id).ToList();
                return tickets;
            }
        }
}

}