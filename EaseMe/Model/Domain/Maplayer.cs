namespace EaseMe.Model.Domain {
    
    public class Maplayer {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual bool isPublic { get; set; }
    }
}
