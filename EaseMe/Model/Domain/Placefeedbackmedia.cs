namespace EaseMe.Model.Domain {
    
    public class Placefeedbackmedia {
        public virtual int id { get; set; }
        public virtual Placefeedback placefeedback { get; set; }
        public virtual byte[] media { get; set; }
        public virtual string description { get; set; }
    }
}
