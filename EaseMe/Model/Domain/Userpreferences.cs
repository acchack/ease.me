namespace EaseMe.Model.Domain {
    
    public class Userpreferences {
        public virtual int id { get; set; }
        public virtual User user { get; set; }
        public virtual Interests interests { get; set; }
        public virtual Transportationmeans transportationmeans { get; set; }
    }
}
