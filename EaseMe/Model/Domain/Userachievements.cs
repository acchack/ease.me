namespace EaseMe.Model.Domain {
    
    public class Userachievements {
        public virtual int id { get; set; }
        public virtual User user { get; set; }
        public virtual Achievements achievements { get; set; }
    }
}
