using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class Ticketsource {
        public Ticketsource() {
			ticket = new List<Ticket>();
        }
        public virtual int id { get; set; }
        public virtual string sourcename { get; set; }
        public virtual string description { get; set; }
        public virtual IList<Ticket> ticket { get; set; }
    }
}
