using System;

namespace EaseMe.Model.Domain {
    
    public class Ticket {
        public virtual int id { get; set; }
        public virtual User user { get; set; }
        public virtual Ticketsource ticketsource { get; set; }
        public virtual Currency currency { get; set; }
        public virtual Ticketstatus ticketstatus { get; set; }
        public virtual int routeid { get; set; }
        public virtual string number { get; set; }
        public virtual DateTime notvalidafter { get; set; }
        public virtual DateTime? notvalidbefore { get; set; }
        public virtual float amount { get; set; }
        public virtual DateTime purchasedon { get; set; }
        public virtual string remarks { get; set; }
        public virtual DateTime createdon { get; set; }
        public virtual DateTime? modifiedon { get; set; }
    }
}
