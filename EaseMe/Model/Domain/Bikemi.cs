using System;
using System.Text;
using System.Collections.Generic;


namespace EaseMe.Model.Domain {
    
    public class Bikemi {
        public virtual int Id { get; set; }
        public virtual string LocationName { get; set; }
        public virtual float Longitude { get; set; }
        public virtual float Latitude { get; set; }
        public virtual float Altitude { get; set; }
    }
}
