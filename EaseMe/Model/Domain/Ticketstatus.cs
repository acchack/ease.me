using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class Ticketstatus {
        public Ticketstatus() {
			ticket = new List<Ticket>();
        }
        public virtual int id { get; set; }
        public virtual string status { get; set; }
        public virtual string description { get; set; }
        public virtual IList<Ticket> ticket { get; set; }
    }
}
