using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class Achievements {
        public Achievements() {
			userachievements = new List<Userachievements>();
        }
        public virtual int id { get; set; }
        public virtual string title { get; set; }
        public virtual string description { get; set; }
        public virtual IList<Userachievements> userachievements { get; set; }
    }
}
