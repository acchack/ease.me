using System;
using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class Placefeedback {
        public Placefeedback() {
			placefeedbackmedia = new List<Placefeedbackmedia>();
        }
        public virtual int id { get; set; }
        public virtual User user { get; set; }
        public virtual int placeid { get; set; }
        public virtual string feedbacktext { get; set; }
        public virtual int rating { get; set; }
        public virtual DateTime submittedon { get; set; }
        public virtual DateTime createdon { get; set; }
        public virtual DateTime? modifiedon { get; set; }
        public virtual IList<Placefeedbackmedia> placefeedbackmedia { get; set; }
    }
}
