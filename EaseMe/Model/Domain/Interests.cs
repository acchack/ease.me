using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class Interests {
        public Interests() {
			userpreferences = new List<Userpreferences>();
        }
        public virtual int id { get; set; }
        public virtual string title { get; set; }
        public virtual string description { get; set; }
        public virtual IList<Userpreferences> userpreferences { get; set; }
    }
}
