using System;
using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class User {
        public User() {
			login = new List<Login>();
			placefeedback = new List<Placefeedback>();
			session = new List<Session>();
			ticket = new List<Ticket>();
			userachievements = new List<Userachievements>();
			userpreferences = new List<Userpreferences>();
        }
        public virtual int id { get; set; }
        public virtual string name { get; set; }
        public virtual string surname { get; set; }
        public virtual int? age { get; set; }
        public virtual string email { get; set; }
        public virtual string gender { get; set; }
        public virtual string location { get; set; }
        public virtual DateTime createdon { get; set; }
        public virtual DateTime? modifiedon { get; set; }
        public virtual IList<Login> login { get; set; }
        public virtual IList<Placefeedback> placefeedback { get; set; }
        public virtual IList<Session> session { get; set; }
        public virtual IList<Ticket> ticket { get; set; }
        public virtual IList<Userachievements> userachievements { get; set; }
        public virtual IList<Userpreferences> userpreferences { get; set; }
    }
}
