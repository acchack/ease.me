using System;

namespace EaseMe.Model.Domain {
    
    public class Session {
        public virtual int id { get; set; }
        public virtual User user { get; set; }
        public virtual string securitytoken { get; set; }
        public virtual DateTime expiry { get; set; }
        public virtual DateTime createdon { get; set; }
        public virtual DateTime? modifiedon { get; set; }
    }
}
