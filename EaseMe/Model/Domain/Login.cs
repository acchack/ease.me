using System;

namespace EaseMe.Model.Domain {
    
    public class Login {
        public virtual int id { get; set; }
        public virtual User user { get; set; }
        public virtual string loginval { get; set; }
        public virtual string password { get; set; }
        public virtual DateTime createdon { get; set; }
        public virtual DateTime? modifiedon { get; set; }
    }
}
