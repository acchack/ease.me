using System.Collections.Generic;

namespace EaseMe.Model.Domain {
    
    public class Transportationmeans {
        public Transportationmeans() {
			userpreferences = new List<Userpreferences>();
        }
        public virtual int id { get; set; }
        public virtual string means { get; set; }
        public virtual string description { get; set; }
        public virtual IList<Userpreferences> userpreferences { get; set; }
    }
}
