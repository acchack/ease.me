﻿namespace EaseMe.Model.Domain
{
    public enum TravelTimeMode
    {
        DepartureTime,
        ArrivalTime
    }
}