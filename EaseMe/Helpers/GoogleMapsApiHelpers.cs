﻿using System;
using System.Collections.Generic;
using System.Linq;
using EaseMe.Controllers;
using EaseMe.Model;
using EaseMe.Model.Domain;
using GoogleMapsApi.Entities.Common;
using GoogleMapsApi.Entities.Directions.Request;
using GoogleMapsApi.Entities.Directions.Response;
using GoogleMapsApi.Entities.Places.Request;
using GoogleMapsApi.Entities.Places.Response;
using NHibernate.Linq;

namespace EaseMe.Helpers
{
    public class GoogleMapsApiHelpers
    {
        public static DirectionsResponse GetDirectionResponse(RoutesController.RouteRequestInfo routeInfo)
        {
            TravelMode travelMode;
            switch (routeInfo.TransportType)
            {
                case 1:
                    travelMode = TravelMode.Bicycling;
                    break;
                case 2:
                    travelMode = TravelMode.Driving;
                    break;
                case 3:
                    travelMode = TravelMode.Transit;
                    break;
                default:
                    travelMode = TravelMode.Walking;
                    break;
            }

            var time = routeInfo.Time;
            TravelTimeMode timeMode;

            if (routeInfo.TimeMode == 0)
            {
                timeMode = TravelTimeMode.ArrivalTime;
            }
            else
            {
                timeMode = TravelTimeMode.DepartureTime;
            }

            return GetDirectionResponse(routeInfo.StartPoint, routeInfo.EndPoint, travelMode, time, timeMode,
                routeInfo.Waypoints);
        }

        private static DirectionsResponse GetDirectionResponse(
            String startPoint,
            String endPoint,
            TravelMode travelMode,
            DateTime travelTime,
            TravelTimeMode travelTimeMode,
            List<String> wayPoints = null
            )
        {
            var directionsRequest = new DirectionsRequest()
            {
                ApiKey = Constants.GoogleMapsApiKey,
                Origin = startPoint,
                Destination = endPoint,
                TravelMode = travelMode
            };

            switch (travelTimeMode)
            {
                case TravelTimeMode.DepartureTime:
                    directionsRequest.DepartureTime = travelTime;
                    break;
                case TravelTimeMode.ArrivalTime:
                    directionsRequest.ArrivalTime = travelTime;
                    break;
            }
            if (wayPoints != null) directionsRequest.Waypoints = wayPoints.ToArray();


            var directions = GoogleMapsApi.GoogleMaps.Directions.Query(directionsRequest);

            return directions;
        }

        public static PlacesResponse GetPlacesResponse(
            Double latitude,
            Double longitude,
            int userId,
            RankBy rankBy)
        {
            return GetPlacesResponse(
                latitude,
                longitude,
                GetUserInterestListFromUserId(userId),
                rankBy);
        }

        private static String GetUserInterestListFromUserId(int userId)
        {
            using (var session = NHibernateSession.OpenSession())
            {
                var userPrefId = session.Query<Userpreferences>();
                var userPref = userPrefId.First(x => x.user.id == userId);
                var userInterestValue = userPref.interests.title;
                return InterestHelper.GetInterestString(userInterestValue);
            }
        }

        private static PlacesResponse GetPlacesResponse(
            Double latitude, 
            Double longitude, 
            String userInterestList,
            RankBy rankBy)
        {
            var placesRequest = new PlacesRequest
            {
                ApiKey = Constants.GoogleMapsApiKey,
                Sensor = false,
                Location = new Location(latitude, longitude),
                Radius = Constants.PlacesSearchRadius,
                Types = userInterestList,
                RankBy = rankBy,
            };
            var places = GoogleMapsApi.GoogleMaps.Places.Query(placesRequest);
            return places;
        }

    }
}