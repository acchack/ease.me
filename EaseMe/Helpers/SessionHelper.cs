﻿using System;
using System.Linq;
using EaseMe.Model;
using EaseMe.Model.Domain;
using NHibernate.Linq;

namespace EaseMe.Helpers
{
    public class SessionHelper
    {
        public static Boolean CreateSession(String _userId, String securityToken)
        {
            var taskSuccess = false;
            using (var session = NHibernateSession.OpenSession())
            {
                var userId = session.Query<Login>();
                userId = userId.Where(x => x.loginval == _userId);
                var userIdValue = userId.First().user.id;
                

              
                using (var transaction = session.BeginTransaction())
                {
                    var userSession = new Session
                    {
                        user = new User {id = userIdValue},
                        securitytoken = securityToken,
                        expiry = DateTime.Now.AddDays(1),
                        createdon = DateTime.Now
                    };
                    taskSuccess = session.Save(userSession) == (object) 1;
                    transaction.Commit();
                }
            }

            return taskSuccess;
        }

        public static Boolean CheckSession(String securityToken)
        {
            var taskSuccess = false;
            using (var session = NHibernateSession.OpenSession())
            {
                var queryResults = session.Query<Session>();
                var res = queryResults.Any(x => x.securitytoken == securityToken);
                taskSuccess = res;
            }
            return taskSuccess;
        }

        public static bool CheckAccessToken(string tokenString)
        {
            return CheckSession(tokenString);
        }
    }

    
}