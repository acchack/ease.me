﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EaseMe.Helpers
{
    public class InterestHelper
    {
        public static String GetInterestString(String interest)
        {
            switch (interest)
            {
                case "History":
                case "Culture":
                case "Architecture":
                    return "cemetery|church|mosque|hindu_temple|synagogue|place_of_worship|courthouse|funeral_home";
                
                case "Music":
                case "Design":
                case "Art":
                    return "amusement_park|aquarium|art_gallery|museum|painter|florist";

                case "Sports":
                    return "bowling_alley|stadium|gym|campground";
                
                case "Travel":
                case "Sightseeing":
                    return
                        "airport|car_dealer|car_rental|car_repair|car_wash|subway_station|taxi_stand|train_station|travel_agency|library|bicycle_store|bus_station|gas_station|embassy|establishment|city_hall|atm|moving_company|park|parking|rv_park|lodging|zoo";
                
                case "Movies":
                    return "movie_rental|movie_theater|atm";
                
                case "Leisure":
                    return "beauty_salon|book_store|night_club|casino|atm|spa";

                case "Food":
                case "Drinking":
                    return "food|bar|bakery|cafe|liquor_store|atm|meal_takeaway|meal_delivery|restaurant";
                
                default:
                    return "airport|car_dealer|car_rental|car_repair|car_wash|subway_station|taxi_stand|train_station|travel_agency|library|bicycle_store|bus_station|gas_station|embassy|establishment|city_hall|atm|moving_company|park|parking|rv_park|lodging|zoo";
            }
        }
    }
}